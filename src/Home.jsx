import React from 'react';
import { Jumbotron, Button } from 'reactstrap';

const Home = (props) => {
  return (
    <div>
      <Jumbotron className="bg-primary col-lg-12 md-6">
        <h1 className="display-3">Welcome to my page!</h1>
        <p className="lead">his is a very simple site created using react.js and reactstrap</p>
        <hr className="my-2" />
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <p className="lead">
          <Button color="light">Learn More</Button>
        </p>
      </Jumbotron>
    </div>
  );
};

export default Home;

