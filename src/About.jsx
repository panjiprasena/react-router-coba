import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import img from './assets/foto-profil.png';
import "./assets/About.css";
import {
    Card, CardImg, CardTitle, CardText, CardDeck, CardBody
  } from 'reactstrap';

const About = (props) => {
  return (
    <CardDeck>
    <Card>
      <CardImg className="favourite-img" src={img} alt="Card image cap" />
      <CardBody>
        <CardTitle tag="h5">Biodata</CardTitle>
        <CardText>
            Name: Panji Bayu Prasena
            <br/>
            Age: 24
            <br/>
            Birthplace: Tulungagung
            <br/>
            Address: Tulungagung, East Java, Indonesia
            <br/>
            Occupation: Student at Glint's Academy #9 FE Class
        </CardText>
      </CardBody>
    </Card>
    <Card>
      <CardBody>
        <CardTitle tag="h5">Summary</CardTitle>
        <CardText>
        “An award-winning and confident communication graduate, able to establish rapport quickly and conduct training sessions with clarity and enthusiasm.”
        </CardText>
      </CardBody>
    </Card>
  </CardDeck>
);
};

export default About;
