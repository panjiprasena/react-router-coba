import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const About = (props) => {
  return (
  <div className="contentCenter">
    <Form>
      <div className="tugasForm">
      <FormGroup>
        <Label for="exampleEmail">Email</Label>
        <Input className="inputForm" type="email" name="email" id="exampleEmail" placeholder="Enter Email" />
      </FormGroup>
      <FormGroup>
        <Label for="examplePassword">Password</Label>
        <Input className="inputForm" type="password" name="password" id="examplePassword" placeholder="Enter the password" />
      </FormGroup>
      <FormGroup>
        <Label for="exampleText">Text Area</Label>
        <Input className="inputForm" type="textarea" name="text" id="exampleText" />
      </FormGroup>
      <FormGroup>
        <Label for="exampleFile">File</Label>
        <Input className="inputForm" type="file" name="file" id="exampleFile" />
        <div className="containeur">
        <FormText color="muted">
          This is some placeholder block-level help text for the above input.
          It's a bit lighter and easily wraps to a new line.
        </FormText>
        </div>
      </FormGroup>
      <FormGroup check>
        <Label check>
          <Input type="checkbox" />{' '}
          Check me out
        </Label>
      </FormGroup>
      <Button>Submit</Button>
      </div >
    </Form>  
  </div>
  );
}

export default About;
