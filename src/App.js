import React from "react";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import { Switch, Route, Link } from "react-router-dom";
import "./styles.css";
import Example from "./navbar"

const App = () => {
  return (
    <div>
      <nav>
       <Example />
      </nav>
    </div>
  );
};

export default App;